# Planet Tor

This repository contains the configuration files and template for Planet Tor.
The website is generated using the Ruby Gem `pluto`, which can be installed via
the Ruby package management system:

    # gem install pluto
